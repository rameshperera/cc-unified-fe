import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { createCustomElement } from '@angular/elements';
import { PublicHolidays } from './public-holidays/public-holidays.es';


@NgModule({
  declarations: [
    AppComponent,
    PublicHolidays
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  // bootstrap: [AppComponent]
  entryComponents: [AppComponent, PublicHolidays]

})
export class AppModule {


  constructor(private injector: Injector) {


    //Define the exported elements

    //public-holidays
    const publicHolidays = createCustomElement(PublicHolidays, { injector });
    customElements.define('public-holidays', publicHolidays);
  }

  ngDoBootstrap() { }
}
