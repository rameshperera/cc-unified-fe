const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
  const files = [
    './dist/cc-unified-app/runtime-es5.js',
    './dist/cc-unified-app/polyfills-es5.js',
    './dist/cc-unified-app/main-es5.js'
  ];

  await fs.ensureDir('components');
  await concat(files, 'components/exported-elements.js');
})();
